import argparse
import imaplib
from email.message import Message
from time import time

URL = ''
LOGIN = ''
PASSWORD = ''

imap = imap = imaplib.IMAP4_SSL(URL)
imap.login(LOGIN, PASSWORD)
imap.select('inbox')

def parse_args():
    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
        description='''
note - requires text in qotes, sends email to itself with given text\n
del - requires email address. emails from it will be moced to 'deleted' folder\n
mark - requires email adress. marks all email from adress
''')
    parser.add_argument(
        '--mode',
        required=True, 
        type=str, 
        nargs=1, 
        choices=['note', 'del', 'mark'])
    parser.add_argument(
        '--val',
        required=True,
        help='email adress or text for \'note\' mode',
        type=str,
        nargs=1)
    parser.add_argument(
       '--subject',
       required=False,
       help='subject for note, only in \'note\' mode',
       type=str,
       nargs=1
    )
    return parser.parse_args()

def main():
    args = parse_args()
    if args.mode == 'note':
        new_message = Message()
        new_message["From"] = LOGIN
        new_message["Subject"] = args.subject
        new_message.set_payload(args.val)

        ok, _ = imap.append(
          'INBOX',
          '',
          imaplib.Time2Internaldate(time()), 
          str(new_message).encode('utf-8')
        )
        assert ok == 'OK'
    elif args.mode == 'del':
      ok, idxs = imap.search(None, f'FROM {args.val}')
      assert ok == 'OK'
      for idx in idxs[0].split():
        imap.store(idx, '+FLAGS', '\\Deleted')
    elif args.mode == 'mark':
        ok, idxs = imap.search(None, f'FROM {args.val}')
        assert ok == 'OK'
        for idx in idxs[0].split():
            imap.store(idx, '+FLAGS', '(\\Flagged)')
        
if __name__ == '__main__':
    main()